/* программа проверяет является ли число простым */
#include <stdio.h>

int main(void)
{
    int number = -10;
    int tmp;
    int i;
    /* если число отрицательное, то проверка на простоту такая же */
    if(number < 0)
        tmp = -1 * number;
    for(i = 2; i < tmp/2; i++){
        if(tmp % i == 0){
            printf("Число %d не простое\n", number);
            return 0;
        }
    }
    printf("Число %d простое\n", number);
    return 0;
}
