/* программа выводит числа от 1 до 100(включительно) с помощью разных циклов */
#include <stdio.h>

int main(void)
{
    int i;
    printf("Вывод циклом for\n");
    for(i = 1; i <= 100; i++){
        printf("%d\n", i);
    }
    printf("Вывод циклом do-while\n");
    i = 1;
    do{
        printf("%d\n", i);
        i++;
    }while(i <= 100);

    printf("Вывод циклом do-while\n");
    i = 1;
    while(i <= 100){
        printf("%d\n", i);
        i++;
    }
    return 0;
}
