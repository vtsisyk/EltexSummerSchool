/* программа считает факториал числа */
#include <stdio.h>

int main(void)
{
    /* факториал именно этого числа будем считать */
    int n = 5;
    int answer = 1;
    if(n < 0){
        printf("Число отрицательное, мне лень\n");
        return 1;
    }
    printf("Исходное число: %d\n", n);
    while(n > 0){
        answer *= n;
        n--;
    }
    printf("Его факториал равен %d\n",  answer);
    return 0;
}
