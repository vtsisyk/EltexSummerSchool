#include <stdio.h>
typedef int (*twoi_t)(int, int);
typedef int (*onei_t)(int);
int sum(int, int);
int mul(int, int);
int sub(int, int);
int mdiv(int, int);
int factorial(int);
