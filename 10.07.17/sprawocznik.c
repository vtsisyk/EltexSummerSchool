#include <stdio.h>
#include <string.h>

typedef struct{
    char name[256];
    char phone[11];
} phonebook_t;

#define SIZE 10
int main()
{
    phonebook_t phonebook[SIZE];
    int choice;
    int del;
    int i = 0;
    char phone[256];
    char name[256];
    char search[256];
    enum options{LIST = 1, NEW, DELETE, SEARCH, EXIT};
    printf("Офигенный справочник может делать:\n\
1. просматривать список\n\
2. добавлять новую запись\n\
3. удалять запись\n\
4. найти по имени\n\
5. выходить\n");
    while(1){
        printf("Что делаем?\n---> ");
        scanf("%d", &choice);
        switch(choice){
            case LIST:
                for(i = 0; i < SIZE; i++)
                    if(phonebook[i].name[0] != '\0'){
                        printf("%s\n", phonebook[i].name);
                        printf("%s\n", phonebook[i].phone);
                        printf("----\n");
                    }
                break;
            case NEW:
                for(i = 0; i < SIZE; i++)
                    if(phonebook[i].name[0] == '\0'){
                        printf("-->");
                        scanf("%s", phonebook[i].name);
                        printf("-->");
                        scanf("%s", phonebook[i].phone);
                        break;
                    }
                if(i == SIZE)
                    printf("Не могу вставить новую запись\n");
                break;
            case DELETE:
                printf("какой удалить? [1-10]? ---> ");
                scanf("%d", &del);
                strcpy(phonebook[del - 1].name, "");
                strcpy(phonebook[del - 1].phone, "");
                break;
            case SEARCH:
                    printf("-->");
                    scanf("%s", search);
                    for(i = 0 ; i < SIZE; i++){
                        if(strstr(phonebook[i].name, search) != NULL){
                            printf("%s\n", phonebook[i].name);
                            printf("%s\n", phonebook[i].phone);
                            printf("----\n");
                        }
                    }
                    break;
            case EXIT:
                return 0;
        }
    }

    return 0;
}
