/* прога для демонстрации выравнивания в структурах */
#include <stdio.h>

int main()
{
    char str[10] = {'a',0,0,0,0,'b',0,0,0,0};
    struct test{
        char a;
        int b;
    } *ptr;

    printf("размер: %d\n", sizeof(struct test));
    ptr = (struct test *)str;
    printf("1 -- %c, %d\n", ptr->a, ptr->b);
    *ptr++;
    printf("2 -- %c, %d\n", ptr->a, ptr->b);

    struct __attribute__((__packed__)) test_align{
        char a;
        int b;
    } *ptr2;

    printf("размер: %d\n", sizeof(struct test_align));
    ptr2 = (struct test_align *) str;
    printf("1 -- %c, %d\n", ptr2->a, ptr2->b);
    *ptr2++;
    printf("2 -- %c, %d\n", ptr2->a, ptr2->b);

    return 0;
}
