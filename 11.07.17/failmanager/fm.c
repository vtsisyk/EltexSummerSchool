#include "fm.h"
int main()
{

    WINDOW *menu_win;
    struct dirent **files;
    char cwd[1024];
    int n_files = 0;
    int i = 0;
    int c;
    char *path = "/home/vlad/";
    int pos = 1;

    chdir(path);
    init_screen();
    /* сделай нормальную сортировку */
    n_files = scandir(path, &files, NULL, alphasort);
    menu_win = newwin(35, 35, 1, 1);
    keypad(menu_win, TRUE);
    wmove(stdscr, LINES - 1, 0);
    wattron(stdscr,A_REVERSE); 
    wprintw(stdscr,  "выход на F1\n");
    wattroff(stdscr,A_REVERSE); 
    refresh();
    print_menu(menu_win, files, n_files, pos);
    while(( c = wgetch(menu_win)) != KEY_F(1)){;
            switch(c){
                case KEY_UP:
                    if(pos == 1)
                        pos = n_files;
                    else
                        --pos;
                    break;
                case KEY_DOWN:
                    if(pos == n_files)
                        pos = 1;
                    else
                        ++pos;
                    break;
                case 10:
                    if(isdir(files[pos- 1]->d_name)){
                        wclear(menu_win);
                        chdir(files[pos - 1]->d_name);
                        for (i = 0; i < n_files; i++){
                            free(files[i]);
                        }
                        free(files);

                        n_files = scandir(".", &files, NULL, alphasort);
                        wrefresh(menu_win);
                        pos = 1;
                        print_menu(menu_win, files, n_files, pos);
                        wmove(stdscr,37,0);
                        clrtoeol();
                        wprintw(stdscr, "%s", getcwd(cwd, sizeof(cwd)));
                        refresh();
                    }
                    break;
                default:
                    refresh();
                    break;
            }
        print_menu(menu_win, files, n_files, pos);
    }
    for (i = 0; i < n_files; i++){
        free(files[i]);
    }
    free(files);
    refresh();
    clrtoeol();
    end_screen();
    return 0;
}

void init_screen()
{
    setlocale(LC_ALL, "");
    initscr();
    start_color();
    use_default_colors();
    noecho();
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    init_pair(2, COLOR_CYAN, -1);
    keypad(stdscr, TRUE);
    curs_set(0);
    raw();

}

void end_screen()
{
    endwin();
}

void print_menu(WINDOW *menu_win, struct dirent **files, int n, int pos)
{
    int x, y, i;
    static int start = 0;
    if(pos < 10)
        start = 0;
    else if((pos %10 == 1) && (start < pos) )
        start = pos + pos % 10;
    else if((pos %10 == 1) && (start >= pos) )
        start = pos - pos % 20;
    x = 2;
    y = 2;
    wclear(menu_win);
    box(menu_win, 0, 0);

    for(i = start; i < n; ++i){
        if(pos == i + 1){
            wattron(menu_win, COLOR_PAIR(1)); 
            mvwprintw(menu_win, y, x, "%s", files[i]->d_name);
            wattroff(menu_win, COLOR_PAIR(1));
        }else{
            if(isdir(files[i]->d_name)){
                wattron(menu_win, COLOR_PAIR(2)); 
                mvwprintw(menu_win, y, x, "%s", files[i]->d_name);
                wattroff(menu_win, COLOR_PAIR(2));
            } else {
                mvwprintw(menu_win, y, x, "%s", files[i]->d_name);
            }
        }
        ++y;
    }
    wrefresh(menu_win);
}

int isdir(const char *path)
{
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
       return 0;
   return S_ISDIR(statbuf.st_mode);
}

