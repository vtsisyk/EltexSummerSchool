#define _GNU_SOURCE
#include <stdlib.h>
#include <dirent.h> 
#include <menu.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <locale.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define MAX_PATH

struct tab{
    /* нынешнее положение*/
    int cur_pos;
}
void init_screen();
void end_screen();
void print_menu(WINDOW *menu_win, struct dirent **files, int n, int highlight);

int isdir(const char *path);
