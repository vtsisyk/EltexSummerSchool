/* простой калькулятор */
#include <stdio.h>
#include "operations.h"

int getNumber();
int main(int argc, char *argv[])
{
    enum {SUM = 1, SUB, MUL, DIV, FACTORIAL, QUIT};
    int choice;
    int var1, var2;
    printf("Офигенный калькуятор может делать следующее: \n\
    1. складывать\n\
    2. вычитать\n\
    3. умножать\n\
    4. делить\n\
    5. считать факториал\n\
    ----\n\
    6. выходить\n");

    while(1){
        printf("Что делаем?\n");
        printf("---> ");
        choice = getNumber();
        switch(choice) {
            case SUM:
                printf("--> ");
                var1 = getNumber();
                printf("--> ");
                var2 = getNumber();
                printf("===> %d\n", sum(var1,var2));
                break;
            case SUB:
                printf("--> ");
                var1 = getNumber();
                printf("--> ");
                var2 = getNumber();
                printf("===> %d\n", sub(var1,var2));
                break;
            case DIV:
                printf("--> ");
                var1 = getNumber();
                printf("--> ");
                var2 = getNumber();
                printf("===> %d\n", mdiv(var1,var2));
                break;
            case MUL:
                printf("--> ");
                var1 = getNumber();
                printf("--> ");
                var2 = getNumber();
                printf("===> %d\n", mul(var1,var2));
                break;
            case FACTORIAL:
                printf("--> ");
                var1 = getNumber();
                printf("===> %d\n", factorial(var1));
                break;
            case QUIT:
                printf("koniec\n");
                /* printf("Конец работы программы\n"); */
                return 0;
        }
    }
    return 0;
}
/* Мы хотим получить только число */
int getNumber()
{
    int num;
    while(1){
        if(scanf("%d", &num) == 1)
            return num;
        getchar();
    }
}
